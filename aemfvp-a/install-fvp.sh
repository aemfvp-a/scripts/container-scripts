#!/bin/sh

# Copyright (c) 2023, ARM Limited and Contributors. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# Neither the name of ARM nor the names of its contributors may be used
# to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

set -e

version=$1
folder=$2
fvps_dir=$3

# Create tools path
mkdir -p ${fvps_dir} && cd ${fvps_dir}

if [ $(uname -m) == "x86_64" ]; then
    url="https://developer.arm.com/-/cdn-downloads/permalink/Fixed-Virtual-Platforms/${folder}/FVP_Base_RevC-2xAEMvA_${version}_Linux64.tgz"
    model="FVP_Base_RevC-2xAEMvA_${version}_Linux64"
else 
    url="https://developer.arm.com/-/cdn-downloads/permalink/Fixed-Virtual-Platforms/${folder}/FVP_Base_RevC-2xAEMvA_${version}_Linux64_armv8l.tgz"
    model="FVP_Base_RevC-2xAEMvA_${version}_Linux64_armv8l"
fi

echo "Downloading ${model}"
wget --no-verbose ${url}

echo "Extracting ${model}"
tar -xf ${model}.tgz

echo "Cleaning files"
rm -rf ${model}.tgz license_terms

